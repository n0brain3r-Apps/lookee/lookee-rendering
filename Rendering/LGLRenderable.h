//
//  LGLRenderable.h
//  Writeability
//
//  Created by Ryan on 10/9/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


@protocol LGLRenderable <NSObject>
@required

- (void)render;

@optional

- (void)renderAtPoint:(CGPoint)point;
- (void)renderInRect:(CGRect)rect;

@end
