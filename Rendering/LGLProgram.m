//
//  LGLProgram.m
//  Writeability
//
//  Created by Ryan on 10/20/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LGLProgram.h"

#import "LGLShader_Private.h"

#import <OpenGLES/ES2/glext.h>



//*********************************************************************************************************************//
#pragma mark -
#pragma mark Externals
#pragma mark -
//*********************************************************************************************************************//




NSString * const kLGLProgramDefaultTexKey = @"global-program-tex_default";
NSString * const kLGLProgramDefaultVBOKey = @"global-program-vbo_default";
NSString * const kLGLProgramColoredTexKey = @"global-program-tex_colored";
NSString * const kLGLProgramSDFieldTexKey = @"global-program-tex_sdfield";




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//





@interface LGLProgramObjectInfo : NSObject
{
@public
    GLint   location;
    GLint   length;
    GLenum  type;
    NSData  *data;
}
@end





//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLProgram
#pragma mark -
// Partially derived from the GPUImage Project @ https://github.com/BradLarson/GPUImage
//*********************************************************************************************************************//




@interface LGLProgram ()

@property (nonatomic, readonly, strong) NSMutableArray      *shaderBin;

@property (nonatomic, readonly, strong) NSMutableDictionary *attributeInfo;
@property (nonatomic, readonly, strong) NSMutableDictionary *uniformInfo;

@property (nonatomic, readonly, assign) GLuint              handle;

@end

@implementation LGLProgram

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
@private
    BOOL _initialized;
}


#pragma mark - Static Objects
//*********************************************************************************************************************//

static const    GLuint      kLGLProgramObjectNameLength = 256;

static __weak   LGLProgram  *vCurrentProgram            = nil;


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (LGLProgram *)current
{
    return vCurrentProgram;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init
{
    if ((self = [super init])) {
        _shaderBin      = [NSMutableArray new];

        _attributeInfo  = [NSMutableDictionary new];
        _uniformInfo    = [NSMutableDictionary new];
    }

    return self;
}

- (void)dealloc
{
    [LGL deleteProgram:_handle];
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setShaders:(NSArray *)shaders
{
    if (![_shaders isEqualToArray:shaders]) {
        [_shaderBin addObjectsFromArray:_shaders];

        {_shaders = shaders;}

        [self reset];
    }
}

- (void)setAttributes:(NSDictionary *)attributes
{
    if (![_attributes isEqualToDictionary:attributes]) {
        [_shaderBin addObjectsFromArray:_shaders];

        {_attributes = attributes;}

        [self reset];
    }
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (BOOL)link
{
    DBGAssert(_handle);

    GLint status;

    glLinkProgram(_handle);
    glGetProgramiv(_handle, GL_LINK_STATUS, &status);

    if (status) {
        /*
         * This block caches GLSL uniform data.
         */
        {
            GLint objects;

            [_uniformInfo removeAllObjects];

            glGetProgramiv(_handle, GL_ACTIVE_UNIFORMS, &objects);

            for (GLint object = 0; object < objects; ++ object) {
                GLsizei nameLength;
                GLchar  name[kLGLProgramObjectNameLength];

                GLint   length;
                GLenum  type;

                glGetActiveUniform(_handle,
                                   object,
                                   kLGLProgramObjectNameLength,
                                   &nameLength,
                                   &length,
                                   &type,
                                   name);

                DBGAssert(nameLength < kLGLProgramObjectNameLength);

                if (nameLength > 0) {
                    LGLProgramObjectInfo *info = [LGLProgramObjectInfo new];
                    info->location  = glGetUniformLocation(_handle, name);
                    info->length    = length;
                    info->type      = type;

                    _uniformInfo[ @(name) ] = info;
                }
            }
        }

        /*
         * This block caches GLSL attribute data.
         */
        {
            GLint objects;

            [_attributeInfo removeAllObjects];

            glGetProgramiv(_handle, GL_ACTIVE_ATTRIBUTES, &objects);

            for (GLint object = 0; object < objects; ++ object) {
                GLsizei nameLength;
                GLchar  name[kLGLProgramObjectNameLength];

                GLint   length;
                GLenum  type;

                glGetActiveAttrib(_handle,
                                  object,
                                  kLGLProgramObjectNameLength,
                                  &nameLength,
                                  &length,
                                  &type,
                                  name);

                DBGAssert(nameLength < kLGLProgramObjectNameLength);

                if (nameLength > 0) {
                    LGLProgramObjectInfo *info = [LGLProgramObjectInfo new];
                    info->location  = glGetAttribLocation(_handle, name);
                    info->length    = length;
                    info->type      = type;

                    _attributeInfo[ @(name) ] = info;
                }
            }
        }
    } else {
#ifdef DEBUG
        GLchar  *info;
		GLsizei infoLength;

        glGetShaderiv(_handle, GL_INFO_LOG_LENGTH, &infoLength);

        info = malloc(infoLength);

        glGetProgramInfoLog(_handle, infoLength, NULL, info);

        NSString *infoLog = @(info);

        free(info);

        DBGWarning(@"Failed to link program (\n%@)", infoLog);
#endif
        return NO;
    }

    return YES;
}

- (BOOL)load
{
    if (!_initialized) {
        if (!_handle) {
            _handle = glCreateProgram();
        }

        for (LGLShader *shader in _shaderBin) {
            if ([shader handle] && ![_shaders containsObject:shader]) {
                glDetachShader(_handle, [shader handle]);
            }
        }

        [_shaderBin removeAllObjects];

        for (LGLShader *shader in _shaders) {
            if (![shader compile]) {
                return NO;
            }

            glAttachShader(_handle, [shader handle]);
        }

        for (NSString *name in _attributes) {
            glBindAttribLocation(_handle, [_attributes[ name ] intValue], [name UTF8String]);
        }

        if (![self link]) {
            return NO;
        }

        {_initialized = YES;}
    }

    return YES;
}

- (BOOL)use
{
    if ([self load]) {
        [LGL setProgram:_handle];

        {vCurrentProgram = self;}
    } else {
        return NO;
    }

    return YES;
}

- (void)reset
{
    {_initialized = NO;}
}

- (void)setData:(NSData *)data forAttributeWithID:(id <NSCopying>)ID
{
    DBGParameterAssert((data != nil) && (ID != nil));

    LGLProgramObjectInfo *info = [self infoForAttributeWithID:ID];

    DBGAssert(info);
    {
        if (![info->data isEqualToData:data]) {
            info->data = [data copy];
            
            for (GLuint index = 0; index < info->length; ++ index) {
                switch (info->type) {
                    case GL_FLOAT: {
                        glVertexAttrib1fv((info->location + index), (GLfloat *)([data bytes] + index));
                    } break;
                    case GL_FLOAT_VEC2: {
                        glVertexAttrib2fv((info->location + index), (GLfloat *)([data bytes] + index));
                    } break;
                    case GL_FLOAT_VEC3: {
                        glVertexAttrib3fv((info->location + index), (GLfloat *)([data bytes] + index));
                    } break;
                    case GL_FLOAT_VEC4: {
                        glVertexAttrib4fv((info->location + index), (GLfloat *)([data bytes] + index));
                    } break;
                    case GL_FLOAT_MAT2: {
                        glVertexAttrib2fv((info->location + index *2 +0), (GLfloat *)([data bytes] + index *2 +0));
                        glVertexAttrib2fv((info->location + index *2 +1), (GLfloat *)([data bytes] + index *2 +2));
                    } break;
                    case GL_FLOAT_MAT3: {
                        glVertexAttrib3fv((info->location + index *3 +0), (GLfloat *)([data bytes] + index *3 +0));
                        glVertexAttrib3fv((info->location + index *3 +1), (GLfloat *)([data bytes] + index *3 +3));
                        glVertexAttrib3fv((info->location + index *3 +2), (GLfloat *)([data bytes] + index *3 +6));
                    } break;
                    case GL_FLOAT_MAT4: {
                        glVertexAttrib4fv((info->location + index *4 +0), (GLfloat *)([data bytes] + index *4 +0 ));
                        glVertexAttrib4fv((info->location + index *4 +1), (GLfloat *)([data bytes] + index *4 +4 ));
                        glVertexAttrib4fv((info->location + index *4 +2), (GLfloat *)([data bytes] + index *4 +8 ));
                        glVertexAttrib4fv((info->location + index *4 +3), (GLfloat *)([data bytes] + index *4 +12));
                    } break;
                }
            }
        }
    }
}

- (void)setData:(NSData *)data forUniformWithID:(id <NSCopying>)ID
{
    DBGParameterAssert((data != nil) && (ID != nil));

    LGLProgramObjectInfo *info = [self infoForUniformWithID:ID];

    DBGAssert(info);
    {
         if (![info->data isEqualToData:data]) {
            info->data = [data copy];
         
            switch (info->type) {
                case GL_SAMPLER_2D:
                case GL_SAMPLER_CUBE: // ???: GL_SAMPLER_CUBE
                case GL_INT: {
                    glUniform1iv(info->location, info->length, (GLint *)[data bytes]);
                } break;
                case GL_INT_VEC2: {
                    glUniform2iv(info->location, info->length, (GLint *)[data bytes]);
                } break;
                case GL_INT_VEC3: {
                    glUniform3iv(info->location, info->length, (GLint *)[data bytes]);
                } break;
                case GL_INT_VEC4: {
                    glUniform4iv(info->location, info->length, (GLint *)[data bytes]);
                } break;
                case GL_FLOAT: {
                    glUniform1fv(info->location, info->length, (GLfloat *)[data bytes]);
                } break;
                case GL_FLOAT_VEC2: {
                    glUniform2fv(info->location, info->length, (GLfloat *)[data bytes]);
                } break;
                case GL_FLOAT_VEC3: {
                    glUniform3fv(info->location, info->length, (GLfloat *)[data bytes]);
                } break;
                case GL_FLOAT_VEC4: {
                    glUniform4fv(info->location, info->length, (GLfloat *)[data bytes]);
                } break;
                case GL_FLOAT_MAT2: {
                    glUniformMatrix2fv(info->location, info->length, GL_FALSE, (GLfloat *)[data bytes]);
                } break;
                case GL_FLOAT_MAT3: {
                    glUniformMatrix3fv(info->location, info->length, GL_FALSE, (GLfloat *)[data bytes]);
                } break;
                case GL_FLOAT_MAT4: {
                    glUniformMatrix4fv(info->location, info->length, GL_FALSE, (GLfloat *)[data bytes]);
                } break;
            }
        }
    }
}

- (void)setProperties:(NSDictionary *)properties forAttributeWithID:(id <NSCopying>)ID
{
    LGLProgramObjectInfo *info = [self infoForAttributeWithID:ID];

    DBGAssert(info);
    {
        glEnableVertexAttribArray(info->location);
        glVertexAttribPointer(info->location,
                              [properties[@"length"     ] intValue          ],
                              [properties[@"type"       ] unsignedIntValue  ],
                              [properties[@"normalized" ] boolValue         ],
                              [properties[@"stride"     ] intValue          ],
                              (GLvoid *)
                              [properties[@"index"      ] longLongValue     ]);
    }
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (LGLProgramObjectInfo *)infoForAttributeWithID:(id)ID
{
    if ([ID isKindOfClass:[NSString class]]) {
        return _attributeInfo[ID];
    }

    if ([ID isKindOfClass:[NSNumber class]]) {
        GLint location = [ID intValue];

        if (location >= 0) {
            for (LGLProgramObjectInfo *info in [_attributeInfo allValues]) {
                if (info->location == location) {
                    return info;
                }
            }
        }
    }

    return nil;
}

- (LGLProgramObjectInfo *)infoForUniformWithID:(id)ID
{
    if ([ID isKindOfClass:[NSString class]]) {
        return _uniformInfo[ID];
    }

    if ([ID isKindOfClass:[NSNumber class]]) {
        GLint location = [ID intValue];

        if (location >= 0) {
            for (LGLProgramObjectInfo *info in [_uniformInfo allValues]) {
                if (info->location == location) {
                    return info;
                }
            }
        }
    }

    return nil;
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Implementations
#pragma mark -
//*********************************************************************************************************************//




@implementation LGLProgramObjectInfo

@end
