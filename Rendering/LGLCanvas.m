//
//  LGLCanvas.m
//  Writeability
//
//  Created by Ryan on 9/25/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LGLTexture_Private.h"

#import "LGLCanvas.h"

#import <OpenGLES/ES2/glext.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLCanvas
#pragma mark -
// Partially derived from the GPUImage Project @ https://github.com/BradLarson/GPUImage
//*********************************************************************************************************************//




@interface LGLCanvas ()

@property (nonatomic, readonly, assign) GLuint mainFrameBuffer;
@property (nonatomic, readonly, assign) GLuint msaaFrameBuffer;
@property (nonatomic, readonly, assign) GLuint msaaRenderBuffer;

@end


@implementation LGLCanvas

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
@private
    BOOL __initialized;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
scale           :(float     )scale
{
    return [self
            initWithWidth   :width
            height          :height
            scale           :scale
            samples         :0];
}

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
depth           :(NSUInteger)depth
scale           :(float     )scale
{
    return [self
            initWithWidth   :width
            height          :height
            depth           :depth
            scale           :scale
            samples         :0];
}

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
scale           :(float     )scale
samples         :(NSUInteger)samples
{
    self = [super
            initWithWidth   :width
            height          :height
            scale           :scale
            data            :nil];

    if (self) {
        [self initializeCanvasWithSamples:samples];
    }

	return self;
}

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
depth           :(NSUInteger)depth
scale           :(float     )scale
samples         :(NSUInteger)samples
{
    self = [super
            initWithWidth   :width
            height          :height
            depth           :depth
            scale           :scale
            data            :nil];

    if (self) {
        [self initializeCanvasWithSamples:samples];
    }

	return self;
}

- (void)initializeCanvasWithSamples:(NSUInteger)samples
{
    GLint maxSamples;
    glGetIntegerv(GL_MAX_SAMPLES_APPLE, &maxSamples);

    _samples = MIN(maxSamples, samples);
}

- (void)dealloc
{
    GLuint mainFrameBuffer  = _mainFrameBuffer;
    GLuint msaaFrameBuffer  = _msaaFrameBuffer;
    GLuint msaaRenderBuffer = _msaaRenderBuffer;
    
	[LGL queueAsyncOperation:^{
		[LGL deleteFrameBuffer:mainFrameBuffer];
        [LGL deleteFrameBuffer:msaaFrameBuffer];
        [LGL deleteRenderBuffer:msaaRenderBuffer];

        glFlush();
	}];
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)clear
{
    [LGL queueAsyncOperation:^{
		if ([self initializeBacking]) {
			if (_samples > 0) {
                [LGL bindFrameBuffer:_msaaFrameBuffer toTarget:GL_FRAMEBUFFER];
            } else {
                [LGL bindFrameBuffer:_mainFrameBuffer toTarget:GL_FRAMEBUFFER];
            }

            [LGL setViewWidth:self.columns height:self.rows];
            [LGL setScale:self.scale];
            [LGL setClearColor:_backgroundColor];

            glClear(GL_COLOR_BUFFER_BIT);
		}
	}];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)queueRenderOperation:(void(^)(void))block
{
	[LGL queueAsyncOperation:^{
		if ([self initializeBacking]) {
            if (_samples > 0) {
                [LGL bindFrameBuffer:_msaaFrameBuffer toTarget:GL_FRAMEBUFFER];
            } else {
                [LGL bindFrameBuffer:_mainFrameBuffer toTarget:GL_FRAMEBUFFER];
            }

            [LGL setViewWidth:self.columns height:self.rows];
            [LGL setScale:self.scale];

            if (block) {
                block();
            }

            if (_samples > 0) {
                [LGL bindFrameBuffer:_msaaFrameBuffer toTarget:GL_READ_FRAMEBUFFER_APPLE];
                [LGL bindFrameBuffer:_mainFrameBuffer toTarget:GL_DRAW_FRAMEBUFFER_APPLE];
                glResolveMultisampleFramebufferAPPLE();
                
                glDiscardFramebufferEXT(GL_READ_FRAMEBUFFER_APPLE, 1, ((GLenum[]){GL_COLOR_ATTACHMENT0}));
            }
		}
	}];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (BOOL)initializeBacking
{
    if (__initialized) {
        return YES;
    }

	glGenFramebuffers(1, &_mainFrameBuffer);

	if (_mainFrameBuffer) {
		if ([self initialize]) {
            [LGL bindFrameBuffer:_mainFrameBuffer toTarget:GL_FRAMEBUFFER];
			[self setAsBacking];

			GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

			if (status == GL_FRAMEBUFFER_COMPLETE) {
				if (_samples > 0) {
					glGenFramebuffers(1, &_msaaFrameBuffer);

					if (_msaaFrameBuffer) {
                        [LGL bindFrameBuffer:_msaaFrameBuffer toTarget:GL_FRAMEBUFFER];
						glGenRenderbuffers(1, &_msaaRenderBuffer);

						if (_msaaRenderBuffer) {
                            [LGL bindRenderBuffer:_msaaRenderBuffer toTarget:GL_RENDERBUFFER];
							glRenderbufferStorageMultisampleAPPLE(GL_RENDERBUFFER,
																  _samples,
																  GL_RGBA8_OES,
																  self.columns,
																  self.rows);

							glFramebufferRenderbuffer(GL_FRAMEBUFFER,
													  GL_COLOR_ATTACHMENT0,
													  GL_RENDERBUFFER,
													  _msaaRenderBuffer);

							status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

							if (status != GL_FRAMEBUFFER_COMPLETE) {
								DBGWarning(@"Failed to make complete framebuffer object (0x%X)", status);

								return NO;
							}
						} else {
							DBGWarning(@"Failed to create anti-aliasing renderbuffer (0x%X)", glGetError());

							return NO;
						}
					} else {
						DBGWarning(@"Failed to create anti-aliasing framebuffer (0x%X)", glGetError());

						return NO;
					}
				}

                {__initialized = YES;}

                [LGL setViewWidth:self.columns height:self.rows];
                [LGL setScale:self.scale];
                [LGL setClearColor:_backgroundColor];

                glClear(GL_COLOR_BUFFER_BIT);
			} else {
				DBGWarning(@"Failed to make complete framebuffer object (0x%X)", status);

				return NO;
			}
		} else {
			return NO;
		}
	} else {
		DBGWarning(@"Failed to create default framebuffer (0x%X)", glGetError());

		return NO;
	}

	return YES;
}

@end
