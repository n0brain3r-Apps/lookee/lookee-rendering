//
//  LGLCanvas.h
//  Writeability
//
//  Created by Ryan on 9/25/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>

#import "LGLTexture.h"



@interface LGLCanvas : LGLTexture

@property (nonatomic, readonly	, assign) NSUInteger    samples;

@property (nonatomic, readwrite	, strong) UIColor       *backgroundColor;


- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
scale           :(float     )scale
samples         :(NSUInteger)samples;

- (instancetype)
initWithWidth   :(NSUInteger)width
height          :(NSUInteger)height
depth           :(NSUInteger)depth
scale           :(float     )scale
samples         :(NSUInteger)samples;


- (void)queueRenderOperation:(void(^)(void))block;

@end
