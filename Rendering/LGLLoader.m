//
//  LGLLoader.m
//  Writeability
//
//  Created by Ryan on 1/16/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//



#import "LGLLoader.h"

#import "LGLProgram.h"
#import "LGLShader.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Shaders
#pragma mark -
//*********************************************************************************************************************//




static NSString * const kLGLShaderAPositionUColor
=
#include "LGLShaderAPositionUColor.h"


static NSString * const kLGLShaderAPositionTexture
=
#include "LGLShaderAPositionTexture.h"


static NSString * const kLGLShaderAPositionUColorTexture
=
#include "LGLShaderAPositionUColorTexture.h"


static NSString * const kLGLShaderAPositionUColorTextureSDF
=
#include "LGLShaderAPositionUColorTextureSDF.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLLoader
#pragma mark -
//*********************************************************************************************************************//




@implementation LGLLoader

#pragma mark - Loader
//*********************************************************************************************************************//

+ (void)load
{
    [LGL queueAsyncOperation:^{
        NSDictionary    *shaders    =
        (@{ kLGLProgramDefaultVBOKey: kLGLShaderAPositionUColor,
            kLGLProgramDefaultTexKey: kLGLShaderAPositionTexture,
            kLGLProgramColoredTexKey: kLGLShaderAPositionUColorTexture,
            kLGLProgramSDFieldTexKey: kLGLShaderAPositionUColorTextureSDF });

        NSCache         *cache      = [NSCache new];

        for (NSString *identifier in shaders) {
            NSString    *shader     = shaders[identifier];
            NSString    *source     = [self loadShaders:@[shader] withCache:cache];

            LGLProgram  *program    = [LGLProgram new];

            program.shaders         =
            (@[
               [LGLShader vertexShaderWithSource:
                @Join(@"#define VERTEX_SHADER", source)],

               [LGLShader fragmentShaderWithSource:
                @Join(@"#define FRAGMENT_SHADER", source)]
               ]);

            DBGAssert([program load]);

            [LGL objectPool][identifier] = program;
        }
    }];
}


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (NSString *)loadShaders:(NSArray *)shaders withCache:(NSCache *)cache
{
    static dispatch_once_t  predicate   = 0;
    static NSArray          *encodings  = nil;

    dispatch_once(&predicate, ^{
        encodings =
        (@[ @(NSUTF8StringEncoding),
            @(NSASCIIStringEncoding),
            @(NSMacOSRomanStringEncoding),
            @(NSNEXTSTEPStringEncoding),
            @(NSWindowsCP1251StringEncoding),
            @(NSWindowsCP1252StringEncoding),
            @(NSWindowsCP1253StringEncoding),
            @(NSWindowsCP1254StringEncoding),
            @(NSWindowsCP1250StringEncoding) ]);
    });

    NSMutableString *processed = [NSMutableString new];

    for (NSString *shader in shaders) {
        [processed appendFormat:@"\n%@\n", shader];
    }

    [processed
     replaceOccurrencesOfString :@"$"
     withString                 :@""
     options                    :0
     range                      :((NSRange) {
        .location   = 0,
        .length     = [processed length]
    })];

    NSError             *error      = nil;

    NSRegularExpression *scanner    =
    [NSRegularExpression
     regularExpressionWithPattern:@"@\\ *([A-z]+[^;]*);"
     options                     :0
     error                       :&error];

    if (!error) {
        [scanner
         replaceMatchesInString :processed
         options                :0
         range                  :((NSRange) {
            .location   = 0,
            .length     = [processed length]
        })
         withTemplate           :@"\n#$1\n"];

        error       = nil;

        scanner     =
        [NSRegularExpression
         regularExpressionWithPattern   :@"#\\ *import\\ *\\(?(\\w+[\\.\\w]*)\\)?"
         options                        :0
         error                          :&error];

        if (!error) {
            NSUInteger  offset      = 0;
            NSString    *process    = [processed copy];

            NSArray     *includes   = [scanner
                                       matchesInString  :process
                                       options          :0
                                       range            :((NSRange) {
                .location   = 0,
                .length     = [process length]
            })];

            for (NSTextCheckingResult *result in includes) {
                NSRange     resultRange = [result range];

                NSString    *include    = [scanner
                                           replacementStringForResult   :result
                                           inString                     :processed
                                           offset                       :offset
                                           template                     :@"$1"];

                NSString    *path       = [[NSBundle mainBundle]
                                           pathForResource  :[include stringByDeletingPathExtension]
                                           ofType           :[include pathExtension]];
                
                NSString    *contents   = [cache objectForKey:path];

                if (!contents) {
                    $for (NSNumber *encoding in encodings) {
                        error       = nil;
                        contents    = [NSString
                                       stringWithContentsOfFile :path
                                       encoding                 :[encoding integerValue]
                                       error                    :&error];

                        if (!error) {
                            [cache setObject:contents forKey:path];
                            break;
                        }
                    } else {
                        DBGWarning(@"Failed to include shader file '%@' (\n%@\n)", include, [error localizedDescription]);

                        contents    = [NSString new];
                    }
                }

                [processed replaceCharactersInRange:resultRange withString:contents];

                resultRange.location += offset;
                offset += ([contents length] - resultRange.length);
            }
        } else {
            DBGWarning(@"Failed to parse shader include directives (\n%@\n)", [error localizedDescription]);
            
            return nil;
        }
    } else {
        DBGWarning(@"Failed to parse shader preprocessor directives (\n%@\n)", [error localizedDescription]);
        
        return nil;
    }
    
    return processed;
}

@end
