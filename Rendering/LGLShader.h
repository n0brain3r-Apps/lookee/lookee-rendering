//
//  LGLShader.h
//  Writeability
//
//  Created by Ryan on 10/20/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LGLObject.h"


@interface LGLShader : LGLObject

@property (nonatomic, readonly, assign, getter = isVertexShader     )   BOOL vertexShader;
@property (nonatomic, readonly, assign, getter = isFragmentShader   )   BOOL fragmentShader;


+ (LGLShader *)vertexShaderWithSource:(NSString *)source;
+ (LGLShader *)fragmentShaderWithSource:(NSString *)source;

- (BOOL)compile;

@end