//
//  LGLShader.m
//  Writeability
//
//  Created by Ryan on 10/20/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LGLShader_Private.h"

#import <OpenGLES/ES2/glext.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGLShader
#pragma mark -
// Partially derived from the CeedGL Project @ https://github.com/rsebbe/CeedGL
//*********************************************************************************************************************//




@interface LGLShader ()

@property (nonatomic, readonly, assign) GLenum type;

@end

@implementation LGLShader

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (LGLShader *)vertexShaderWithSource:(NSString *)source
{
    return [LGLShader.alloc initWithType:GL_VERTEX_SHADER source:source];
}

+ (LGLShader *)fragmentShaderWithSource:(NSString *)source
{
    return [LGLShader.alloc initWithType:GL_FRAGMENT_SHADER source:source];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init __ILLEGALMETHOD__;

- (instancetype)initWithType:(GLenum)type source:(NSString *)source
{
    if ((self = [super init])) {
        _type           = type;
        _handle         = glCreateShader(type);

        _vertexShader   = (type == GL_VERTEX_SHADER);
        _fragmentShader = (type == GL_FRAGMENT_SHADER);

        if (_handle) {
            const char *bytes = [source UTF8String];
            
            glShaderSource(_handle, 1, &bytes, NULL);
        } else {
            DBGWarning(@"Failed to create shader (0x%X)", glGetError());

            return nil;
        }
    }

    return self;
}

- (void)dealloc
{
    if (_handle) {
        glDeleteShader(_handle);
    }
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (BOOL)compile
{
	glCompileShader(_handle);

	GLint result;
	glGetShaderiv(_handle, GL_COMPILE_STATUS, &result);

	if (!result) {
#ifdef DEBUG
		GLchar  *info;
		GLsizei infoLength;

        glGetShaderiv(_handle, GL_INFO_LOG_LENGTH, &infoLength);

        info = malloc(infoLength);

		glGetShaderInfoLog(_handle, infoLength, NULL, info);

        NSString *infoLog = @(info);

        free(info);

		DBGWarning(@"Failed to compile shader (\n%@)", infoLog);
#endif
		return NO;
	}

    return YES;
}

@end